package com.incept.api.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incept.api.common.Response;
import com.incept.api.services.InterestService;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:inceptdbApplicationContext.xml")
public class InterestDaoTest {
	
	@Autowired
	private InterestService interestService;
	
	@Test
	public void testGetAllInterest() {
		
		Response interestResponse = interestService.allInterest();
		//System.out.println("Interest ==> "+interestResponse);
		Assert.assertNotNull(interestResponse);
	}

}
