package com.incept.api.vo;


public class InterestVO {
	
	private long interestId;
	private String interestName;
	
	public long getInterestId() {
		return interestId;
	}
	public void setInterestId(long interestId) {
		this.interestId = interestId;
	}
	public String getInterestName() {
		return interestName;
	}
	public void setInterestName(String interestName) {
		this.interestName = interestName;
	}
	
	@Override
	public String toString() {
		return "InterestVO [interestId=" + interestId + ", interestName=" + interestName + "]";
	}	
}
