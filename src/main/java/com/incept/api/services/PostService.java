package com.incept.api.services;

import com.incept.api.common.Response;
import com.incept.api.domain.requests.PostRequest;

public interface PostService {

	Response post(PostRequest request);

	Response getPost(String profileId, String postId);

	Response delete(String profileId, String postId);

}
