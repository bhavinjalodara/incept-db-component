package com.incept.api.services;

import com.incept.api.common.Response;
import com.incept.api.domain.requests.GetProfileRequest;
import com.incept.api.domain.requests.SignupRequest;
import com.incept.api.domain.requests.UpdateProfileRequest;

public interface ProfileService {

	public Response updateProfile(UpdateProfileRequest request);

	public Response getProfile(GetProfileRequest request);

	public Response deactivateProfile(GetProfileRequest request);
	
	public Response signup(SignupRequest request);

}
