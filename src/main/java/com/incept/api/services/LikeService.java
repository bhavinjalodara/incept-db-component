package com.incept.api.services;

import com.incept.api.common.Response;
import com.incept.api.domain.requests.LikeRequest;

public interface LikeService {

	Response createlike(LikeRequest request);

	Response getLike(String postId, String profileId);

}
