package com.incept.api.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incept.api.common.Response;
import com.incept.api.dao.LikeDao;
import com.incept.api.domain.requests.LikeRequest;
import com.incept.api.model.Like;
import com.incept.api.services.LikeService;
import com.incept.api.services.enums.DateFormatter;
import com.incept.api.utils.InceptUtils;
import com.incept.api.vo.LikeVO;

@Service
@Transactional
public class LikeServiceImpl implements LikeService {

	Logger logger = LoggerFactory.getLogger(LikeServiceImpl.class);

	@Autowired
	private LikeDao likeDao;

	@Override
	public Response createlike(LikeRequest request) {
		logger.info("start like service Impl .");
		Response response = new Response();
		Like like = new Like();
		like.setPostId(request.getPostId());
		like.setProfileId(request.getProfileId());
		like.setCreatedTime(InceptUtils.getDateInString(
				DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
		like.setModifiedTime(InceptUtils.getDateInString(
				DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
		like.setType(request.getType());
		List<Like> listofLike = likeDao.getLike(request.getPostId(),
				request.getProfileId());
		if (listofLike.isEmpty()) {
			likeDao.createLike(like);
		} else {
			boolean update = likeDao.updateLike(like);
			if (update) {
				response.setStatusCode(1);
				response.setMessage("record successfully updated !");
			}
		}
		response.setStatusCode(1);
		response.setMessage("success");
		logger.info("end like service Impl. ");
		return response;
	}

	@Override
	public Response getLike(String postId, String profileId) {
		logger.info(" start getLike in service Impl layer ");
		Response response = new Response();
		try {
			List<LikeVO> listLikeRes = new ArrayList<LikeVO>();
			logger.info(" start getLike from database .");
			List<Like> likes = likeDao.getLike(postId, profileId);
			if (likes != null) {
				logger.info(" record form likes in size : " + likes.size());
				for (Like like : likes) {
					LikeVO res = new LikeVO();
					res.setPostId(postId);
					res.setProfileId(profileId);
					res.setType(like.getType());
					listLikeRes.add(res);
				}
				response.setResponse(listLikeRes);
				response.setStatusCode(1);
				response.setMessage("success");
				logger.info(" like in success ");
			} else {
				logger.info(" like in error ");
				response.setMessage("error");
				response.setStatusCode(0);
			}
			logger.info(" end getLike in service Impl layer . ");
		} catch (Exception e) {
			logger.error(" get error while getting like service .", e);
			response.setMessage("error");
			response.setStatusCode(0);
		}
		logger.info(" end get like from service implementation layer ");
		return response;
	}

}
