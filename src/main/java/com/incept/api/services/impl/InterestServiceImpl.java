package com.incept.api.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.incept.api.common.Response;
import com.incept.api.common.ResponseCode;
import com.incept.api.dao.InterestDao;
import com.incept.api.domain.responses.InterestResponse;
import com.incept.api.model.Interest;
import com.incept.api.services.InterestService;
import com.incept.api.vo.InterestVO;

@Service("interestService")
public class InterestServiceImpl implements InterestService {

	@Autowired
	private InterestDao interestDao;

	@Transactional(readOnly = true)
	public Response allInterest() {
		List<Interest> interestdao = interestDao.getAllInterest();
		Response response = getInterestVOResponse(interestdao);
		return response;
	}

	private Response getInterestVOResponse(List<Interest> interestdao) {
		List<InterestVO> listOfInterest = new ArrayList<InterestVO>();
		Response response = new Response();
		for (Interest interest : interestdao) {
			InterestVO interestVO = new InterestVO();
			interestVO.setInterestId(interest.getInterestId());
			interestVO.setInterestName(interest.getInterestName());
			listOfInterest.add(interestVO);
		}
		InterestResponse interestResponse = new InterestResponse();
		interestResponse.setInterests(listOfInterest);
		response.setResponse(interestResponse);
		response.setStatusCode(ResponseCode.SUCCESS.getCode());
		response.setMessage(String.valueOf(listOfInterest.size()));
		return response;
	}

	/**
	 * Returns all Root interests
	 */
	@Override
	public List<Interest> getAllRootInterest() {
		return interestDao.getAllRootInterest();
	}

	/**
	 * Returns child interests
	 * 
	 */
	@Override
	public List<Interest> getInterestsByParentId(long parentInterestId) {
		return interestDao.getInterestsByParentId(parentInterestId);
	}

	/**
	 * Return interest by Id
	 */
	@Override
	public Interest getInterestById(long interestId) {
		return interestDao.getInterest(interestId);
	}

/*	public InterestResponse getInterest(Integer interestId) {
		InterestResponse response = new InterestResponse();
		Interest interest = interestDao.getInterest(interestId);
		if (interest != null && interest.getInterestName() != null) {
			InterestVO interestVO = new InterestVO();
			interestVO.setInterestId(interest.getInterestId());
			interestVO.setInterestName(interest.getInterestName());
			response.setInterest(interestVO);
		}
		return response;
	}*/
}
