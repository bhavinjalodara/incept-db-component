package com.incept.api.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incept.api.common.Response;
import com.incept.api.dao.CommentDao;
import com.incept.api.dao.ProfileDao;
import com.incept.api.domain.responses.CommentResponse;
import com.incept.api.domain.requests.CommentRequest;
import com.incept.api.model.CommentDetails;
import com.incept.api.services.CommentService;
import com.incept.api.services.enums.ActionType;
import com.incept.api.services.enums.CommentStatus;
import com.incept.api.services.enums.DateFormatter;
import com.incept.api.utils.InceptUtils;
@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	Logger logger = LoggerFactory.getLogger(CommentServiceImpl.class);

	@Autowired
	CommentDao commentDao;

	@Autowired
	ProfileDao profileDao;

	@Override
	public Response comment(CommentRequest request) {
		logger.debug(" service for comment .");
		Response response = null;
		boolean isProfileIdValid = getProfileIdFromDB(request.getProfileId());
		if (isProfileIdValid) {
			CommentDetails details = new CommentDetails();
			details.setCommentText(request.getCommentText());
			details.setProfileId(request.getProfileId());
			details.setPostId(Long.parseLong(request.getPostId()));
			// details.setAgent(request.getAgent());
			// details.setIp(request.getIp());
			details.setModifiedDate(InceptUtils.getDateInString(
					DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
			String type = request.getType();
			if (ActionType.NEW.getStatus().equals(type)) {
				details.setCreatedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setCommentStatus(CommentStatus.APPROVED);
			} else if (ActionType.UPDATE.getStatus().equals(type)) {
				details.setModifiedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setCommentStatus(CommentStatus.APPROVED);
				details.setCommentId(Long.parseLong(request.getCommentId()));
			} else if (ActionType.DELETE.getStatus().equals(type)) {
				details.setDeletedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setCommentStatus(CommentStatus.DELETED);
			} else if (ActionType.DRAFT.getStatus().equals(type)) {
				details.setCreatedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setCommentStatus(CommentStatus.DRAFT);
			} else if (ActionType.UPDATEDRAFT.getStatus().equals(type)) {
				details.setModifiedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setCommentStatus(CommentStatus.DRAFT);
				details.setCommentId(Long.parseLong(request.getCommentId()));
			}
			response = commentDao.comment(details);
			logger.debug(" service for Post");
		} else {
			response = new Response();
			response.setMessage(" No Profile found ! ");
			response.setStatusCode(0);
		}
		return response;
	}

	private boolean getProfileIdFromDB(String profileId) {
		boolean validProfile = false;
		long profile = profileDao.getProfileId(Long.parseLong(profileId));
		if (profile > 0) {
			validProfile = true;
		}
		return validProfile;
	}

	@Override
	public Response getComments(String profileId, String postId) {
		logger.info("start service for comment .");
		Response response = null;
		boolean isProfileIdValid = getProfileIdFromDB(profileId);
			try {
				List<CommentDetails> commentResponse = commentDao
						.getComments(postId);
				List<CommentResponse> responseComments = getCommentResponse(
						commentResponse);
				if (responseComments != null) {
					response = new Response();
					response.setMessage(
							String.valueOf(responseComments.size()));
					response.setStatusCode(1);
					response.setResponse(responseComments);
				}
			} catch (Exception e) {
				logger.error("An error occured while retrieving comments", e);
				response = new Response();
				response.setMessage(e.getMessage());
				response.setStatusCode(0);
			}
			logger.info(" end service for Comment");
		return response;
	}

	private List<CommentResponse> getCommentResponse(List commentResponselist) {
		List<CommentResponse> listPostResponse = null;
		CommentResponse response;
		if (!commentResponselist.isEmpty()) {
			listPostResponse = new ArrayList<CommentResponse>();
			Object[] row;
			for (Object object : commentResponselist) {
				row = (Object[]) object;
				response = new CommentResponse();
				response.setCommentText((String) row[0]);
				response.setProfileId(String.valueOf(row[1]));
				response.setCommentId(String.valueOf(row[2]));
				response.setTime((String) row[3]);
				listPostResponse.add(response);
			}
		}

		return listPostResponse;

	}

}
