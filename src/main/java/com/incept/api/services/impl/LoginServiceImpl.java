package com.incept.api.services.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incept.api.common.Response;
import com.incept.api.dao.LoginDao;
import com.incept.api.dao.LoginDetailsDao;
import com.incept.api.services.LoginService;
import com.incept.api.vo.LoginVO;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Autowired
	private LoginDao loginDao;

	@Autowired
	private LoginDetailsDao loginDetailsDao;

	public Response login(LoginVO login) {
		Response response = new Response();;
		long profileId = getProfileId(login);
		try {
			if (profileId != 0) {
				logger.info("user is valid with userid " + login.getUserId()
						+ " and related profile id is " + profileId);
				login.setProfileId(profileId);

				String key = loginDao.setkeyData(login);
				if (login.getPassword().equals(key)) {
					logger.debug("password matched !.");
					logger.info(" save the details for Login track ");
					String loginKey = loginDetailsDao.saveLoginDetail(login);
					if (loginKey != null) {
						logger.info("update record in user table while.");
						loginDao.updateLoginDetail(login.getProfileId(),
								loginKey);
						response.setResponse(loginKey);
						response.setStatusCode(1);
						response.setMessage("SUCCESS");
					} else {
						logger.info(" Some problem while fetching token key");
						response.setStatusCode(0);
						response.setMessage("ERROR");
					}
				} else {
					logger.debug(" key is incorrect . ");
					response.setStatusCode(0);
					response.setMessage("ERROR");
				}
			} else {
				logger.info(" User is not valid for userId" + login.getUserId());
				response.setStatusCode(0);
				response.setMessage("ERROR");
			}
		} catch (Exception e) {
			logger.error("Error while performing database. ", e);
			response.setStatusCode(0);
			response.setMessage("ERROR");
		}
		return response;
	}

	private long getProfileId(LoginVO login) {
		logger.info(" start getting profile from service layer ");
		logger.debug(" user id is " + login.getUserId());
		long profileId = 0;
		try {
			profileId = loginDao.getProfileId(login.getUserId());
		} catch (Exception e) {
			logger.error("An error occurred while retrieving profile Id", e);
		}

		return profileId;
	}

}
