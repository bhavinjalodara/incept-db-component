package com.incept.api.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incept.api.common.Response;
import com.incept.api.dao.InterestDao;
import com.incept.api.dao.PostDao;
import com.incept.api.dao.ProfileDao;
import com.incept.api.domain.requests.PostRequest;
import com.incept.api.domain.responses.PostResponse;
import com.incept.api.exception.DAOException;
import com.incept.api.model.PostDetails;
import com.incept.api.services.PostService;
import com.incept.api.services.enums.ActionType;
import com.incept.api.services.enums.DateFormatter;
import com.incept.api.services.enums.PostStatus;
import com.incept.api.utils.InceptUtils;

@Service
@Transactional
public class PostServiceImpl implements PostService {

	Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

	@Autowired
	PostDao postDao;

	@Autowired
	ProfileDao profileDao;
	@Autowired
	InterestDao interestDao;
	@Override
	public Response post(PostRequest request) {
		logger.info(" service for Post .");
		Response response = null;
		boolean isProfileIdValid = getProfileIdFromDB(request.getProfileId());

		if (isProfileIdValid) {
			PostDetails details = null;
			String type = request.getType();
			if (ActionType.DELETE.getStatus().equals(type)
					|| ActionType.UPDATE.getStatus().equals(type)
					|| ActionType.UPDATEDRAFT.getStatus().equals(type)) {
				try {
					details = postDao.getPostbyID(request.getProfileId(),
							request.getPostId());
				} catch (DAOException e) {
					logger.error("An error occurred while retrieing POST", e);
					response = new Response();
					response.setMessage(e.getMessage());
					response.setStatusCode(0);
					return response;
				}
			} else {
				details = new PostDetails();
				details.setPostText(request.getPostText());
				details.setProfileId(request.getProfileId());
			}
			details.setModifiedDate(InceptUtils.getDateInString(
					DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
			if (request.getInterests() != null) {
				String[] interests = request.getInterests().split(",");
				List<Integer> interestList = new ArrayList<Integer>();
				for (String interest : interests) {
					interestList.add(Integer.parseInt(interest));
				}
				details.setInterests(interestDao.getInterestSet(interestList));
			}
			if (ActionType.NEW.getStatus().equals(type)) {
				details.setCreatedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setPostStatus(PostStatus.APPROVED);
			} else if (ActionType.UPDATE.getStatus().equals(type)) {
				details.setPostStatus(PostStatus.APPROVED);
			} else if (ActionType.DELETE.getStatus().equals(type)) {
				details.setDeletedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setPostStatus(PostStatus.DELETED);
			} else if (ActionType.DRAFT.getStatus().equals(type)) {
				details.setCreatedDate(InceptUtils.getDateInString(
						DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
				details.setPostStatus(PostStatus.DRAFT);
			} else if (ActionType.UPDATEDRAFT.getStatus().equals(type)) {
				details.setPostId(Long.parseLong(request.getPostId()));
				details.setPostStatus(PostStatus.DRAFT);
			}
			response = postDao.post(details);
			logger.info(" service for Post");
		} else {
			response = new Response();
			response.setMessage(" No Profile found ! ");
			response.setStatusCode(0);
		}
		return response;
	}

	private boolean getProfileIdFromDB(String profileId) {
		boolean validProfile = false;
		long profile = profileDao.getProfileId(Long.parseLong(profileId));
		if (profile > 0) {
			validProfile = true;
		}
		return validProfile;
	}

	@Override
	public Response getPost(String profileId, String postId) {
		Response response = new Response();
		boolean isValidProfile = getProfileIdFromDB(profileId);
		if (isValidProfile) {
			List<PostDetails> postResponse = postDao.getPost(profileId, postId);
			List<PostResponse> responsePost = getPostResponse(postResponse);
			if (responsePost != null) {
				response.setMessage("success");
				response.setStatusCode(1);
				response.setResponse(responsePost);
			} else {
				response.setMessage("Error");
				response.setStatusCode(0);
			}
		} else {
			response.setStatusCode(0);
			response.setMessage("Profile Id is not valid ! ");
		}
		return response;
	}

	private List<PostResponse> getPostResponse(
			List<PostDetails> postResponselist) {
		List<PostResponse> listPostResponse = null;
		if (!postResponselist.isEmpty()) {
			listPostResponse = new ArrayList<PostResponse>();
			for (PostDetails postDetail : postResponselist) {
				PostResponse response = new PostResponse();
				response.setPostText(postDetail.getPostText());
				listPostResponse.add(response);
			}
		}
		return listPostResponse;

	}

	@Override
	public Response delete(String profileId, String postId) {
		Response response = new Response();
		boolean isValidProfile = getProfileIdFromDB(profileId);
		if (isValidProfile) {
			boolean isdeleted = postDao.delete(profileId, postId);
			if (isdeleted) {
				response.setStatusCode(1);
				response.setMessage("Post is deleted Successfully");
			} else {
				response.setStatusCode(0);
				response.setMessage("Error");
			}
		} else {
			response.setStatusCode(0);
			response.setMessage(" profile not found ! ");
		}
		return response;
	}

}
