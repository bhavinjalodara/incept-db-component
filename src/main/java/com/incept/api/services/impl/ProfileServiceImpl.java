package com.incept.api.services.impl;

import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.incept.api.common.Response;
import com.incept.api.common.ResponseCode;
import com.incept.api.dao.ProfileDao;
import com.incept.api.domain.requests.GetProfileRequest;
import com.incept.api.domain.requests.SignupRequest;
import com.incept.api.domain.requests.UpdateProfileRequest;
import com.incept.api.domain.responses.GetProfileResponse;
import com.incept.api.domain.responses.SignupResponse;
import com.incept.api.services.ProfileService;
import com.incept.api.utils.InceptUtils;

@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	ProfileDao profileDao;

	Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

	@Override
	public Response updateProfile(UpdateProfileRequest request) {
		logger.info("start update Profile in Service Layer ");
		Response response = new Response();
		try {
			String error = profileDao.updateProfile(request);
			if (StringUtils.isEmpty(error)) {
				response.setStatusCode(ResponseCode.SUCCESS.getCode());
				response.setMessage("Profile updated successfully for : profile id "
						+ request.getProfileId());
			} else {
				response.setStatusCode(ResponseCode.FAILURE.getCode());
				response.setMessage(error);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(ResponseCode.FAILURE.getCode());
			response.setMessage(e.getMessage());
		}
		logger.info("End update Profile in Service Layer ");
		return response;
	}

	@Override
	public Response getProfile(GetProfileRequest request) {
		logger.info(" for getProfile in LoginServiceImpl");
		Response response = new Response();

		try {
			GetProfileResponse resp = profileDao.getProfile(request);
			if (resp != null) {
				response.setStatusCode(ResponseCode.SUCCESS.getCode());
				response.setMessage("successfully retived information for profile : "
						+ request.getProfileId());
				response.setResponse(resp);
			} else {
				response.setStatusCode(ResponseCode.FAILURE.getCode());
				response.setMessage(ResponseCode.FAILURE.getDescription());
			}
		} catch (Exception e) {
			logger.error("An error occurred while retriveing profile", e);
			response.setStatusCode(ResponseCode.FAILURE.getCode());
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@Override
	public Response deactivateProfile(GetProfileRequest request) {
		logger.info("Deactivate User Profile for " + request.getProfileId());
		Response response = profileDao.deactivateProfile(request);
		return response;
	}
	
	@Override
	@Transactional
	public Response signup(SignupRequest request) {
		logger.debug("start for Sign up in ProfileServiceImpl");
		Response response = new Response();
		try {
			String uid = UUID.randomUUID().toString()+System.currentTimeMillis();
			//String userToken = UUID.fromString(name).toString();
			request.setPassword(InceptUtils.encryptd(request.getPassword()));
			String profileId = profileDao.signup(request, uid);
			response.setResponse(new SignupResponse(profileId, uid));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatusCode(ResponseCode.FAILURE.getCode());
			if (e instanceof ConstraintViolationException) {
				response.setMessage("This email Id is already exists. ");
			} else {
				response.setMessage(e.getMessage());
			}
		}
		logger.debug("End for Sign up in ProfileServiceImpl");
		return response;
	}

}
