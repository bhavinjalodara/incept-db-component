
package com.incept.api.services;

import com.incept.api.common.Response;
import com.incept.api.domain.requests.CommentRequest;

public interface CommentService {
	
	Response comment(CommentRequest request);

	Response getComments(String profileId,String postId);
}
