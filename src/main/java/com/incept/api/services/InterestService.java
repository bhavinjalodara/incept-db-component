package com.incept.api.services;

import java.util.List;

import com.incept.api.common.Response;
import com.incept.api.model.Interest;

public interface InterestService {

	Response allInterest();

	/*
	 * Retrurn all the Root categories
	 */
	public List<Interest> getAllRootInterest();

	public List<Interest> getInterestsByParentId(long parentInterestId);

	public Interest getInterestById(long interestId);

}
