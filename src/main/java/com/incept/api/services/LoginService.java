package com.incept.api.services;

import com.incept.api.common.Response;
import com.incept.api.vo.LoginVO;

public interface LoginService {

	public Response login(LoginVO login);

}
