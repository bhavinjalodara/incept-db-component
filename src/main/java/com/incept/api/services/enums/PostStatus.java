package com.incept.api.services.enums;

public enum PostStatus {

	DRAFT("draft"), APPROVED("approved"), BLOCKED("blocked"), REJECTED(
			"rejected"), DELETED("deleted");

	private String status;

	private PostStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}
