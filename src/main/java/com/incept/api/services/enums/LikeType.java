package com.incept.api.services.enums;

public enum LikeType {
	
	UP_VOTE("upvote"),DOWN_VOTE("downvote");
	
	private String type;
	
	

	private LikeType(String type) {
		this.type = type;
	}



	public String getType() {
		return type;
	}
	
	

}
