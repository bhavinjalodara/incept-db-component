package com.incept.api.services.enums;

public enum CommentStatus {

	DRAFT("draft"), APPROVED("approved"), BLOCKED("blocked"), REJECTED(
			"rejected"), DELETED("deleted");

	private String status;

	private CommentStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}
