package com.incept.api.services.enums;

public enum AccountStatus {
	
	SUSPEND("suspend"),ACTIVE("active"),INACTIVE("inactive");
	
	private AccountStatus(String status) {
		this.status = status;
	}

	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
