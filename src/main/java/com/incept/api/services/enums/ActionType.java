package com.incept.api.services.enums;

public enum ActionType {
	NEW("0"), UPDATE("1"),DELETE("2"),DRAFT("3"),UPDATEDRAFT("4");
	private ActionType(String status) {
		this.status = status;
	}

	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
