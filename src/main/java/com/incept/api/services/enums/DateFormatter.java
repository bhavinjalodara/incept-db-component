package com.incept.api.services.enums;

public enum DateFormatter {

	DATE_IN_SECOND("yyyy-MM-dd hh:mm:ss"), DATE_IN_MILI_SECOND(
			"yyyy-MM-dd hh:mm:ss.S"), DATE_IN_YYMMDD("yyyy-MM-dd"), DATE_IN_MMDDYYYY(
			"MM-dd-yyyy");

	private String format;

	private DateFormatter(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
