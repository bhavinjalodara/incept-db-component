package com.incept.api.exception;

public class DAOException extends Exception{
	private static final long serialVersionUID = 4810288226265471719L;
	private String errorMessage;

	public DAOException(String error) {
		errorMessage = error;
	}

	@Override
	public String getMessage() {
		return errorMessage;
	}
}
