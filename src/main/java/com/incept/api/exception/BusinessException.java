package com.incept.api.exception;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4810288226265471719L;
	private String errorMessage;

	public BusinessException(String error) {
		errorMessage = error;
	}

	@Override
	public String getMessage() {
		return errorMessage;
	}

}
