package com.incept.api.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

public class EncryptionRSA {

	public byte[] rsaEncrypt(final byte[] data) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		final PublicKey pubKey = readKeyFromFile("/public.key");
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		final byte[] cipherData = cipher.doFinal(data);
		return cipherData;
	}

	public String encrypt(final String rawText, final String publicKeyPath)
			throws IOException, GeneralSecurityException {

		final X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
				IOUtils.toByteArray(new FileInputStream(publicKeyPath)));

		final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance("RSA").generatePublic(x509EncodedKeySpec));

		return Base64.encodeBase64String(cipher.doFinal(rawText.getBytes("UTF-8")));
	}
	
	public String encrypt(final String rawText, final InputStream publicKeyInputStream)
			throws IOException, GeneralSecurityException {

		final X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(IOUtils.toByteArray(publicKeyInputStream));

		final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance("RSA").generatePublic(x509EncodedKeySpec));

		return Base64.encodeBase64String(cipher.doFinal(rawText.getBytes("UTF-8")));
	}

	public String decrypt(final String cipherText, final String privateKeyPath)
			throws IOException, GeneralSecurityException {

		final PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
				IOUtils.toByteArray(new FileInputStream(privateKeyPath)));

		final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance("RSA").generatePrivate(pkcs8EncodedKeySpec));

		return new String(cipher.doFinal(Base64.decodeBase64(cipherText)), "UTF-8");
	}

	public String decrypt(final String cipherText, final PrivateKey privateKey, final String transformation,
			final String encoding) throws IOException, GeneralSecurityException {

		final Cipher cipher = Cipher.getInstance(transformation);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return new String(cipher.doFinal(Base64.decodeBase64(cipherText)), encoding);
	}

	public byte[] rsaDecrypt(final byte[] data) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		final PrivateKey privKey = readPrivateKeyFromFile("/private.key");
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privKey);
		final byte[] cipherData = cipher.doFinal(data);
		return cipherData;

	}

	PublicKey readKeyFromFile(final String keyFileName) throws IOException {
		final URL url = getClass().getResource(keyFileName);
		final InputStream in = url.openStream();
		final ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
		try {

			final BigInteger m = (BigInteger) oin.readObject();
			final BigInteger e = (BigInteger) oin.readObject();
			final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
			final KeyFactory fact = KeyFactory.getInstance("RSA");
			final PublicKey pubKey = fact.generatePublic(keySpec);
			return pubKey;
		} catch (final Exception e) {
			throw new RuntimeException("Spurious serialisation error", e);
		} finally {
			oin.close();
		}
	}

	PrivateKey readPrivateKeyFromFile(final String keyFileName) throws IOException {
		final URL url = getClass().getResource(keyFileName);
		final InputStream in = url.openStream();
		final ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
		try {

			final BigInteger m = (BigInteger) oin.readObject();
			final BigInteger e = (BigInteger) oin.readObject();
			final RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
			final KeyFactory fact = KeyFactory.getInstance("RSA");
			final PrivateKey priKey = fact.generatePrivate(keySpec);
			return priKey;
		} catch (final Exception e) {
			throw new RuntimeException("Spurious serialisation error", e);
		} finally {
			oin.close();
		}
	}

}
