package com.incept.api.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class InceptUtils {
	/* All the method must be static beacause it is a util package */
	
	private static Logger logger = LoggerFactory.getLogger(InceptUtils.class);

	public static Date getDateInFormat(final String dateFormater,
			final Date date1) {
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormater);
		try {
			date = formatter.parse(formatter.format(date1));
		} catch (ParseException ex) {
			logger.error("Date Parsing Error : " + ex);
		}
		return date;
	}

	public static String getDateInString(final String dateFormater,
			final Date date1) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormater);
		return formatter.format(date1);
	}
	
	public static String encryptd(String plaintext) 
	{
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error occurrd while encrypting", e);
		}
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}
		return hashtext;
	}
}
