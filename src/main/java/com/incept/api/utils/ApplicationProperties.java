package com.incept.api.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.incept.api.common.Constant;

@Deprecated
public abstract class ApplicationProperties {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);
	private static Properties applicationProperties = new Properties();

	/*static{
		String methodName = "static block::";
		try {
			loadPropertiesFile();
		} catch (IOException e) {
			logger.error(methodName + "could not load application properties");
			logger.error(methodName + e.getMessage());
		}
	}

	public static void loadPropertiesFile() throws IOException{
		String methodName = "loadPropertiesFile::";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(Constant.DB_FILE_PATH);
			applicationProperties.load(fis);
		}catch (IOException e) {
			logger.error(methodName + "could not load application properties");
			logger.error(methodName + e.getMessage());
		}
		finally {
			if (fis != null) {
				fis.close();
			}
		}
	}

	public static String getProperty(final String key){
		String methodName = "getProperty::";
		String value = applicationProperties.getProperty(key);
		if(null == value || value.length() <= 0){
			logger.error(methodName + key + ", key was not found");
		}
		return value;
	}*/
}
