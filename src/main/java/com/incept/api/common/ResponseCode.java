package com.incept.api.common;

public enum ResponseCode {

	FAILURE(0, "No interest found", ""), ABC(23, "23", "23"), SERVER_EXCEPTION(401, "Exception",
			"Some Issue while connecting to server "), SUCCESS(1,"success",""),INTERNAL_SERVER_ERROR(500,"Exception", "Internal Server Error"
					),;
	
	
	private Integer code;
	private String description;
	private String esbErrorCode;

	ResponseCode(final Integer code, final String description,
			final String esbErrorCode) {
		this.description = description;
		this.code = code;
		this.esbErrorCode = esbErrorCode;
	}

	public String getDescription() {
		return description;
	}

	public Integer getCode() {
		return code;
	}

	public String getEsbErrorCode() {
		return esbErrorCode;
	}

	public static ResponseCode getResponseCodeValue(final String esbErrorCode) {
		for (ResponseCode responseCode : ResponseCode.values()) {
			if (responseCode.getEsbErrorCode().equalsIgnoreCase(esbErrorCode)) {
				return responseCode;
			}
		}
		return null;
	}

}
