package com.incept.api.common;

public class Response {
	
	
	private int statusCode=1;
	private String message = "";
	private Object response = null;
	
	public Response() {
	}

	public Response(int statusCode, String message, Object response) {
		this.statusCode = statusCode;
		this.message = message;
		this.response = response;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public Response setStatusCode(int statusCode) {
		this.statusCode = statusCode;
		return this;
	}
	public String getMessage() {
		return message;
	}
	public Response setMessage(String message) {
		this.message = message;
		return this;
	}
	public Object getResponse() {
		return response;
	}
	public Response setResponse(Object response) {
		this.response = response;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Response [statusCode=").append(statusCode).append(", message=").append(message)
				.append(", response=").append(response).append("]");
		return builder.toString();
	}
}
