package com.incept.api.domain.responses;

public class SignupResponse {

	private String profileId = "";
	private String oAuthToken = "";

	public SignupResponse(String profileId, String oAuthToken) {
		this.profileId = profileId;
		this.oAuthToken = oAuthToken;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getOauthtoken() {
		return oAuthToken;
	}

	public void setOauthtoken(String oAuthToken) {
		this.oAuthToken = oAuthToken;
	}
}
