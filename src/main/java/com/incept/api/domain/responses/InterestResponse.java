package com.incept.api.domain.responses;

import java.util.List;

import com.incept.api.vo.InterestVO;

public class InterestResponse{
	
	private List<InterestVO> interests;
	
	private InterestVO interest;

	public List<InterestVO> getInterests() {
		return interests;
	}

	public void setInterests(List<InterestVO> interests) {
		this.interests = interests;
	}

	public InterestVO getInterest() {
		return interest;
	}

	public void setInterest(InterestVO interest) {
		this.interest = interest;
	}

	@Override
	public String toString() {
		return "InterestResponse [interests=" + interests + ", interest=" + interest + "]";
	}
}
