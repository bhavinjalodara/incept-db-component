package com.incept.api.domain.responses;

import java.io.Serializable;

public class CommentResponse implements Serializable{
	

	private static final long serialVersionUID = -2932540813685477139L;
	private String commentId;
	private String commentText;
	private String profileId;
	private String time;
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getCommentId() {
		return commentId;
	}
	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
