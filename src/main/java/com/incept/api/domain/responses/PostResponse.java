package com.incept.api.domain.responses;

import java.io.Serializable;

public class PostResponse implements Serializable{
	
	private String postId;
	private String postText;
	private String profileId;
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getPostText() {
		return postText;
	}
	public void setPostText(String postText) {
		this.postText = postText;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	
	
	

}
