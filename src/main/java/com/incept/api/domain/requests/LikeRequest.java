package com.incept.api.domain.requests;

import java.io.Serializable;

import com.incept.api.services.enums.LikeType;

public class LikeRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6939525658872859811L;
	private String postId;
	private String profileId;
	private LikeType type;
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public LikeType getType() {
		return type;
	}
	public void setType(LikeType type) {
		this.type = type;
	}
	
}
