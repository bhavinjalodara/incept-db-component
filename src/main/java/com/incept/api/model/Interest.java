package com.incept.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Table
@Entity(name="INTEREST")
public class Interest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1023825L;

	@Column(name="INTRST_NAME")
	private String interestName;
	
	@Id
	@Column(name="INTRST_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long interestId;

	@Column(name="STATUS")
	private String status;

	@Column(name="interestImageId")
	@ColumnDefault("0")
	private long interestImageId;

	@Column(name="parentInterestId")
	@ColumnDefault("0")
	private long parentInterestId;

	public String getInterestName() {
		return interestName;
	}
	public void setInterestName(String interestName) {
		this.interestName = interestName;
	}
	public long getInterestId() {
		return interestId;
	}
	public void setInterestId(long interestId) {
		this.interestId = interestId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getInterestImageId() {
		return interestImageId;
	}
	public void setInterestImageId(long interestImageId) {
		this.interestImageId = interestImageId;
	}
	public long getParentInterestId() {
		return parentInterestId;
	}
	public void setParentInterestId(long parentInterestId) {
		this.parentInterestId = parentInterestId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Interest [interestName=").append(interestName).append(", interestId=").append(interestId)
				.append(", status=").append(status).append(", interestImageId=").append(interestImageId)
				.append(", parentInterestId=").append(parentInterestId).append("]");
		return builder.toString();
	}

}
