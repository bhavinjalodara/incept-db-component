package com.incept.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.incept.api.services.enums.LikeType;

@Entity
@Table(name="LIKE")
public class Like implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 187987L;
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Column(name="POST_ID")
	private String postId;
	@Column(name="PROFILE_ID")
	private String profileId;
	@Column(name="TYPE")
	private LikeType type;
	@Column(name="CREATED_TIME")
	private String createdTime;
	@Column(name="MODIFIED_TIME")
	private String modifiedTime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	
	public LikeType getType() {
		return type;
	}
	public void setType(LikeType type) {
		this.type = type;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Override
	public String toString() {
		return "Like [id=" + id + ", postId=" + postId + ", profileId=" + profileId + ", type=" + type
				+ ", createdTime=" + createdTime + ", modifiedTime=" + modifiedTime + "]";
	}
}
