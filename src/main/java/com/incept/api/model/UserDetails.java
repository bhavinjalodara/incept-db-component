package com.incept.api.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.incept.api.services.enums.AccountStatus;

@Table
@Entity(name = "USER_DETAILS")
public class UserDetails implements Serializable {

	private static final long serialVersionUID = 10002424L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long profile_id;
	@Column(name = "FIRST_NAME")
	private String fname;
	@Column(name = "LAST_NAME")
	private String lname;
	@Column(name = "EMAIL_ID", unique = true)
	private String emailId;
	@Column(name = "DOB")
	private String dob;
	@Column(name = "SEX")
	private String sex;
	
	@Column(name = "LAST_LOGIN_KEY")
	private String lastLoginKey;

	@Column(name = "CREATED_DATE")
	private String createdDate;
	@Column(name = "MODIFIED_DATE")
	private String modifiedDate;
	
	@Column(name = "IS_EXPIRED")
	private Boolean is_expired;
	@Column(name = "EXPIRED_DATE")
	private String expiredDate;
	@Column(name = "STATUS")
	private AccountStatus status;
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "EMAIL_VERIFIED")
	private boolean isEmailVerified;
	
	@Column(name = "USER_TOKEN")
	private String token;

	@Column(name = "LOGIN_TOKEN_CREATION_DATE")
	private String loginTokenCreatedDate;
	

	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_INTEREST_MAPPING", joinColumns = { @JoinColumn(name = "PROFILE_ID") }, inverseJoinColumns = { @JoinColumn(name = "INTEREST_ID") })
	private Set<Interest> interests = new HashSet<Interest>();


	public String getLastLoginKey() {
		return lastLoginKey;
	}

	public void setLastLoginKey(String lastLoginKey) {
		this.lastLoginKey = lastLoginKey;
	}

	public boolean isEmailVerified() {
		return isEmailVerified;
	}

	public void setEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getLoginTokenCreatedDate() {
		return loginTokenCreatedDate;
	}

	public void setLoginTokenCreatedDate(String loginTokenCreatedDate) {
		this.loginTokenCreatedDate = loginTokenCreatedDate;
	}

	public Set<Interest> getInterests() {
		return interests;
	}

	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getProfile_id() {
		return profile_id;
	}

	public void setProfile_id(long profile_id) {
		this.profile_id = profile_id;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Boolean getIs_expired() {
		return is_expired;
	}

	public void setIs_expired(Boolean is_expired) {
		this.is_expired = is_expired;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UserDetails [profile_id=" + profile_id + ", fname=" + fname + ", lname=" + lname + ", emailId="
				+ emailId + ", dob=" + dob + ", sex=" + sex + ", lastLoginKey=" + lastLoginKey + ", createdDate="
				+ createdDate + ", modifiedDate=" + modifiedDate + ", is_expired=" + is_expired + ", expiredDate="
				+ expiredDate + ", status=" + status + ", password=" + password + ", isEmailVerified=" + isEmailVerified
				+ ", token=" + token + ", loginTokenCreatedDate=" + loginTokenCreatedDate + ", interests=" + interests
				+ "]";
	}
}
