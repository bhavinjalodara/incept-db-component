package com.incept.api.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.incept.api.services.enums.PostStatus;

@Table
@Entity(name = "POST_DETAILS")
public class PostDetails implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8335060152146480840L;

	@Id
	@Column(name="POST_ID",updatable=false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long postId;
	
	@Column(name="PROFILE_ID",updatable=false)
	private String profileId;
	
	@Column(name="POST_MESSAGE")
	private String postText;
	
	@Column(name="POST_STATUS")
	private PostStatus postStatus;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "POST_INTEREST_MAPPING", joinColumns = { @JoinColumn(name = "POST_ID") }, inverseJoinColumns = { @JoinColumn(name = "INTEREST_ID") })
	private Set<Interest> interests = new HashSet<Interest>();
	
	@Column(name="CREATED_DATE",updatable=false)
	private String createdDate;
	
	@Column(name="MODIFIED_DATE")
	private String modifiedDate;
	
	@Column(name="DELETED_DATE")
	private String deletedDate;
	
	@Column(name="AGENT")
	private String agent;
	
	@Column(name="IP_ADDRESS")
	private String ip;
	
	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getPostText() {
		return postText;
	}
	public void setPostText(String postText) {
		this.postText = postText;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public PostStatus getPostStatus() {
		return postStatus;
	}
	public void setPostStatus(PostStatus postStatus) {
		this.postStatus = postStatus;
	}
	public String getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(String deletedDate) {
		this.deletedDate = deletedDate;
	}
	public Set<Interest> getInterests() {
		return interests;
	}
	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}

	@Override
	public String toString() {
		return "PostDetails [postId=" + postId + ", profileId=" + profileId + ", postText=" + postText + ", postStatus="
				+ postStatus + ", interests=" + interests + ", createdDate=" + createdDate + ", modifiedDate="
				+ modifiedDate + ", deletedDate=" + deletedDate + ", agent=" + agent + ", ip=" + ip + "]";
	}
}
