
package com.incept.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
import com.incept.api.services.enums.CommentStatus;

@Table
@Entity(name = "COMMENT_DETAILS")
public class CommentDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1023225L;
	@Id
	@Column(name = "COMMENT_ID",updatable=false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long commentId;

	@Column(name = "POST_ID",updatable=false)
	private long postId;

	@Column(name = "PROFILE_ID",updatable=false)
	private String profileId;

	@Column(name = "COMMENT_MESSAGE")
	private String commentText;

	@Column(name = "COMMENT_STATUS")
	private CommentStatus commentStatus;

	@Column(name = "AGENT")
	private String agent;

	@Column(name = "IP_ADDRESS")
	private String ip;

	@Column(name="CREATED_DATE",updatable=false)
	private String createdDate;
	
	@Column(name="MODIFIED_DATE")
	private String modifiedDate;
	
	@Column(name="DELETED_DATE")
	private String deletedDate;

	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}


	public CommentStatus getCommentStatus() {
		return commentStatus;
	}
	public void setCommentStatus(CommentStatus commentStatus) {
		this.commentStatus = commentStatus;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public long getCommentId() {
		return commentId;
	}
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(String deletedDate) {
		this.deletedDate = deletedDate;
	}
	@Override
	public String toString() {
		return "CommentDetails [commentId=" + commentId + ", postId=" + postId + ", profileId=" + profileId
				+ ", commentText=" + commentText + ", commentStatus=" + commentStatus + ", agent=" + agent + ", ip="
				+ ip + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", deletedDate="
				+ deletedDate + "]";
	}

}
