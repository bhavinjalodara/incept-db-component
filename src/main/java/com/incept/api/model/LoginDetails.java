package com.incept.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "LOGIN_DETAILS")
public class LoginDetails {

	@Column(name = "PROFILE_ID")
	private long profileId;
	@Id
	@Column(name = "LOGIN_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long loginId;
	@Column(name = "LOGIN_KEY")
	private String loginKey;
	@Column(name = "CREATED_DATE")
	private String createdDate;
	@Column(name = "STATUS")
	private String status;

	public long getProfileId() {
		return profileId;
	}
	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}
	public long getLoginId() {
		return loginId;
	}
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	public String getLoginKey() {
		return loginKey;
	}
	public void setLoginKey(String loginKey) {
		this.loginKey = loginKey;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LoginDetails [profileId=" + profileId + ", loginId=" + loginId + ", loginKey=" + loginKey
				+ ", createdDate=" + createdDate + ", status=" + status + "]";
	}
}
