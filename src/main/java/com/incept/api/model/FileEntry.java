package com.incept.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name="FileEntry")
public class FileEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3158671435851733266L;

	@Id
	@Column(name="fileEntryId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long fileEntryId;

	@Column(name="fileName")
	private String fileName;

	@Column(name="filePath")
	private String filePath;
	
	@Column(name="fileExtention")
	private String fileExtention;

	/**
	 * @return the fileEntryId
	 */
	public long getFileEntryId() {
		return fileEntryId;
	}

	/**
	 * @param fileEntryId the fileEntryId to set
	 */
	public void setFileEntryId(long fileEntryId) {
		this.fileEntryId = fileEntryId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileExtention
	 */
	public String getFileExtention() {
		return fileExtention;
	}

	/**
	 * @param fileExtention the fileExtention to set
	 */
	public void setFileExtention(String fileExtention) {
		this.fileExtention = fileExtention;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileEntry [fileEntryId=").append(fileEntryId).append(", fileName=").append(fileName)
				.append(", filePath=").append(filePath).append(", fileExtention=").append(fileExtention).append("]");
		return builder.toString();
	}

}
