package com.incept.api.dao;

import java.util.List;

import com.incept.api.common.Response;
import com.incept.api.exception.BusinessException;
import com.incept.api.model.CommentDetails;

public interface CommentDao {
	Response comment(CommentDetails request);

	List<CommentDetails> getComments(String postId) throws BusinessException;

	long getCommentCount(String postId);
}
