package com.incept.api.dao;

import com.incept.api.common.Response;
import com.incept.api.domain.requests.GetProfileRequest;
import com.incept.api.domain.requests.SignupRequest;
import com.incept.api.domain.requests.UpdateProfileRequest;
import com.incept.api.domain.responses.GetProfileResponse;

public interface ProfileDao {

	public String updateProfile(UpdateProfileRequest request) throws Exception;

	public GetProfileResponse getProfile(GetProfileRequest request) throws Exception;

	public Response deactivateProfile(GetProfileRequest request);

	public long getProfileId(Long profileId);
	
	public String signup(SignupRequest request,String loginToken) throws Exception;

}
