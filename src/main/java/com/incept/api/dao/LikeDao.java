package com.incept.api.dao;

import java.util.List;

import com.incept.api.model.Like;

public interface LikeDao {

	void createLike(Like like);

	List<Like> getLike(String postId, String profileId);

	Boolean updateLike(Like like);

}
