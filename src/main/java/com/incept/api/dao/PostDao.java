package com.incept.api.dao;

import java.util.List;

import com.incept.api.common.Response;
import com.incept.api.exception.DAOException;
import com.incept.api.model.PostDetails;

public interface PostDao {

	Response post(PostDetails postDetails);

	List<PostDetails> getPost(String profileId, String postId);

	boolean delete(String profileId, String postId);
	
	public PostDetails getPostbyID(String profileId, String postId) throws DAOException;

}
