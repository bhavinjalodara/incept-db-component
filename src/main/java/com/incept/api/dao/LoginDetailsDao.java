package com.incept.api.dao;

import com.incept.api.vo.LoginVO;

public interface LoginDetailsDao {
	
	String saveLoginDetail(LoginVO login);

}
