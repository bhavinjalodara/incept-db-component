package com.incept.api.dao;

import com.incept.api.vo.LoginVO;

public interface LoginDao {
	
	
	public Object login(String userName,String password);

	/**
	 * For Signup when user create then there in one to many mapping with user_details and Interest 
	 * @param request
	 */

	public long getProfileId(String userId) throws Exception;

	public String setkeyData(LoginVO login);

	public void updateLoginDetail(long profileId,String loginKey);

}
