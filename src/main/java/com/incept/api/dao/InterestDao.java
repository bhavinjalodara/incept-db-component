package com.incept.api.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import com.incept.api.model.Interest;

public interface InterestDao {

	public List<Interest> getAllInterest();

	public Interest getInterest(long interestId) throws HibernateException;

	public Set<Interest> getInterestSet(List<Integer> interestList);

	public List<Interest> getAllRootInterest();

	public List<Interest> getInterestsByParentId(long parentInterestId);

}
