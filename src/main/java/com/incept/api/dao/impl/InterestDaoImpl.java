package com.incept.api.dao.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.dao.InterestDao;
import com.incept.api.model.Interest;
import com.incept.api.services.enums.AccountStatus;

@Repository
public class InterestDaoImpl implements InterestDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Interest> getAllInterest() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Interest.class).add(Restrictions.eq("status", "ACTIVE"));
		List<Interest> list = criteria.list();

		return list;
	}

	@Override
	public Interest getInterest(long interestId) throws HibernateException{
		Session session = getSession();
		Criteria criteria = session.createCriteria(Interest.class).add(Restrictions.eq("interestId", interestId))
				.add(Restrictions.eq("status", AccountStatus.ACTIVE.getStatus()));
		Interest interest = (Interest) criteria.uniqueResult();

		return interest;
	}
	
	public Set<Interest> getInterestSet(List<Integer> interestList) {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Interest.class).add(Restrictions.eq("status", AccountStatus.ACTIVE.getStatus()))
				.add(Restrictions.in("interestId", interestList.toArray()));
		Set<Interest> interestSet = new HashSet<Interest>(criteria.list());

		return interestSet;
	}
	
	@Override
	public List<Interest> getAllRootInterest() {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Interest.class).add(Restrictions.eq("parentInterestId", 0L))
				.add(Restrictions.eq("status", AccountStatus.ACTIVE.getStatus()));
		return criteria.list();
	}

	@Override
	public List<Interest> getInterestsByParentId(long parentInterestId) {
		Session session = getSession();
		Criteria criteria = session.createCriteria(Interest.class).add(Restrictions.eq("parentInterestId", parentInterestId))
				.add(Restrictions.eq("status", AccountStatus.ACTIVE.getStatus()));
		return criteria.list();
	}

	private Session getSession() {
		try {
		    return sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
		    return sessionFactory.openSession();
		}
	}
}
