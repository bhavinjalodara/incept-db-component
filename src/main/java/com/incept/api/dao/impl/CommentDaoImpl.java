package com.incept.api.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.exception.BusinessException;
import com.incept.api.common.Response;
import com.incept.api.dao.CommentDao;
import com.incept.api.model.CommentDetails;
import com.incept.api.services.enums.CommentStatus;

@Repository
public class CommentDaoImpl implements CommentDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = LoggerFactory.getLogger(CommentDaoImpl.class);

	@Override
	public Response comment(CommentDetails request) {
	 	Response response=null;
		Session session = sessionFactory.getCurrentSession();
		try {
			session.saveOrUpdate(request);
			response = new Response();
			response.setMessage("Comment updated successfully : "+request.getCommentId());
			response.setStatusCode(1);
		} catch (Exception e) {
			logger.error(" while saving comment having some Problem ", e);
			response = new Response();
			response.setMessage("Problem while commenting !"+e.getMessage());
			response.setStatusCode(0);
		}
		return response;
	}

	@Override
	public List<CommentDetails> getComments(String postId) throws BusinessException {
		Session session = sessionFactory.getCurrentSession();
		List<CommentDetails> commentList = Collections.emptyList();
		try {
			Query query = session.createSQLQuery("SELECT cd.PROFILE_ID, cd.COMMENT_MESSAGE, cd.COMMENT_ID,cd.MODIFIED_DATE"
					+ " FROM COMMENT_DETAILS cd, USER_DETAILS ud WHERE cd.POST_ID =:postId AND cd.PROFILE_ID = ud.ID "
					+ "AND cd.COMMENT_STATUS =1 AND ud.STATUS =1").setParameter("postId", Long.parseLong(postId));

					 commentList = query.list();
		} catch (Exception e) {
			logger.error("An error occurred while retrieving commnets", e);
			throw new BusinessException("Error while executing query");
		}
		return commentList;
	}

	@Override
	public long getCommentCount(String postId) {
		Session session = sessionFactory.getCurrentSession();
		long count=0;
		try {
			Criteria creteria = session
					.createCriteria(CommentDetails.class)
					.add(Restrictions.eq("postId", postId))
					.add(Restrictions.eq("commentStatus",
							CommentStatus.APPROVED));
			creteria.setProjection(Projections.rowCount());
			count = (long) creteria.uniqueResult();
		} catch (Exception e) {
			logger.error("An error occurred while retrieving commnets counts", e);
		}
		return count;
	}
}
