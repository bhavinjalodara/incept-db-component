package com.incept.api.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.common.Response;
import com.incept.api.dao.PostDao;
import com.incept.api.exception.DAOException;
import com.incept.api.model.PostDetails;
import com.incept.api.services.enums.PostStatus;

@Repository
public class PostDaoImpl implements PostDao {

	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = LoggerFactory.getLogger(PostDaoImpl.class);

	@Override
	public Response post(PostDetails postDetails) {
		Response response = new Response();;
		Session session = sessionFactory.getCurrentSession();
		try {
			session.saveOrUpdate(postDetails);
			response.setMessage("Post updated successfully : " + postDetails.getPostId());
			response.setStatusCode(1);
		} catch (Exception e) {
			logger.error(" while saving post having some Problem ", e);
			response.setMessage("Problem while posting !" + e.getMessage());
			response.setStatusCode(0);
		}
		return response;
	}

	@Override
	public List<PostDetails> getPost(String profileId, String postId) {
		Session session = sessionFactory.getCurrentSession();
		List<PostDetails> postList = null;
		try {
			Criteria creteria = session.createCriteria(PostDetails.class).add(Restrictions.eq("profileId", profileId))
					.add(Restrictions.eq("postId", postId)).add(Restrictions.eq("postStatus", PostStatus.APPROVED));
			postList = (List<PostDetails>) creteria.list();
		} catch (Exception e) {
			logger.error("An error occurred while retriveing post", e);
		}
		return postList;
	}
	@Override
	public PostDetails getPostbyID(String profileId, String postId) throws DAOException {
		Session session = sessionFactory.getCurrentSession();
		List<PostDetails> postList = null;
		try {
			Criteria creteria = session.createCriteria(PostDetails.class).add(Restrictions.eq("profileId", profileId))
					.add(Restrictions.eq("postId", postId)).add(Restrictions.eq("postStatus", PostStatus.APPROVED));
			postList = (List<PostDetails>) creteria.list();
			return postList.get(0);
		} catch (Exception e) {
			throw new DAOException(e.getMessage());
		}
	}

	@Override
	public boolean delete(String profileId, String postId) {
		boolean isDeleted = false;
		Session session = sessionFactory.getCurrentSession();
		try {
			String hqlUpdate = "delete from  PostDetails c where c.profileId = :profileId and c.postId=:postId";
			int updatedEntities = session.createQuery(hqlUpdate).setString("postId", postId)
					.setString("profileId", profileId).executeUpdate();
			if (updatedEntities != 0) {
				isDeleted = true;
			}
		} catch (Exception e) {
			isDeleted = false;
		}
		return isDeleted;
	}
}
