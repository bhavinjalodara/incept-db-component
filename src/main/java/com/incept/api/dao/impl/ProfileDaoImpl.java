package com.incept.api.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.common.Constant;
import com.incept.api.common.Response;
import com.incept.api.dao.InterestDao;
import com.incept.api.dao.ProfileDao;
import com.incept.api.domain.requests.GetProfileRequest;
import com.incept.api.domain.requests.SignupRequest;
import com.incept.api.domain.requests.UpdateProfileRequest;
import com.incept.api.domain.responses.GetProfileResponse;
import com.incept.api.exception.BusinessException;
import com.incept.api.model.Interest;
import com.incept.api.model.UserDetails;
import com.incept.api.services.enums.AccountStatus;
import com.incept.api.services.enums.DateFormatter;
import com.incept.api.utils.InceptUtils;

@Repository
public class ProfileDaoImpl implements ProfileDao {

	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = LoggerFactory.getLogger(ProfileDaoImpl.class);

	@Autowired
	InterestDao interestDao;

	@Override
	public String updateProfile(UpdateProfileRequest request) throws Exception {
		logger.info("updateProfile for LoginDao Implementation .");
		Session session = sessionFactory.getCurrentSession();
		UserDetails userDetails = null;
		try {
			userDetails = (UserDetails) session.get(UserDetails.class, request.getProfileId());
			if (userDetails == null) {
				return "No profile found";
			} else {
				if (!request.getEmail().equals(userDetails.getEmailId())) {
					Criteria criteria = session.createCriteria(UserDetails.class).add(Restrictions.eq("emailId", userDetails.getEmailId()))
							.add(Restrictions.ne("id", userDetails.getProfile_id()));

					criteria.setProjection(Projections.rowCount());
					Long count = (Long) criteria.uniqueResult();
					if (count > 0) {
						return "Email Id Already Exists";
					} else {
						userDetails.setEmailId(request.getEmail());
					}
				}
			}
		} catch (ObjectNotFoundException e) {
			return "No profile found.";
		} catch (Exception e) {
			return "update Failed.";
		}

		if (request.getDob() != null) {
			userDetails.setDob(request.getDob());
		}
		if (request.getFname() != null) {
			userDetails.setFname(request.getFname());
		}
		if (request.getLname() != null) {
			userDetails.setLname(request.getLname());
		}
		if (request.getSex() != null) {
			userDetails.setSex(request.getSex());
		}
		if (request.getData() != null) {
			userDetails.setPassword(request.getData());
		}

		if (request.getInterestlist() != null) {
			String[] interests = request.getInterestlist().split(",");
			List<Integer> interestList = new ArrayList<Integer>();
			for (String interest : interests) {
				interestList.add(Integer.parseInt(interest));
			}
			if (interests.length >= Constant.MIN_INTEREST) {
				Set<Interest> interestSet = interestDao.getInterestSet(interestList);
				if (interestSet.size() < Constant.MIN_INTEREST) {
					return "duplicate interest or wrong id.";
				}
				userDetails.setInterests(interestSet);
			} else {
				return "Minimum 5 interest must be present.";
			}
		}
		userDetails.setModifiedDate(InceptUtils.getDateInString("yyyyy-MM-dd hh:mm:ss", new Date()));
		return "";
	}

	@Override
	public GetProfileResponse getProfile(GetProfileRequest request)
			throws Exception {
		logger.info("getProfile for LoginDao Implementation .");
		List<UserDetails> userDetailsList = null;
		if (request.getEmail() != null || request.getProfileId() != 0) {
			Session session = sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(UserDetails.class);

			cr.add(Restrictions.eq("emailId", request.getEmail())).add(Restrictions.eq("profile_id", request.getProfileId()));
			userDetailsList = cr.list();
		}
		if (userDetailsList == null || userDetailsList.size() == 0) {
			throw new BusinessException("No profile found matching criteria.");
		} else {
			UserDetails userDetails = userDetailsList.get(0);
			GetProfileResponse resp = new GetProfileResponse();
			resp.setProfileId(userDetails.getProfile_id());
			// resp.setUserid(userDetails.getUserid());
			resp.setFname(userDetails.getFname());
			resp.setLname(userDetails.getLname());
			resp.setDob(userDetails.getDob());
			resp.setEmail(userDetails.getEmailId());
			resp.setSex(userDetails.getSex());
			Set<Interest> interestSet = userDetails.getInterests();
			if (interestSet != null) {
				StringBuilder sb = new StringBuilder();
				for (Interest inter : interestSet) {
					sb.append(inter.getInterestId()).append(",");
				}
				resp.setInterestlist(sb.toString());
			}
			return resp;
		}
	}

	@Override
	public Response deactivateProfile(GetProfileRequest request) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("emailId", request.getEmail())).add(Restrictions.eq("profile_id", request.getProfileId()))
				.add(Restrictions.eq("status", AccountStatus.ACTIVE.getStatus()));
		UserDetails userDetailList = (UserDetails) cr.uniqueResult();
		if (userDetailList != null) {

		}
		return null;
	}

	@Override
	public long getProfileId(Long profileId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UserDetails.class).add(Restrictions.eq("profile_id", profileId))
				.add(Restrictions.eq("status", AccountStatus.ACTIVE)).setProjection(Projections.property("profile_id"));
		long pro_id = (long) criteria.uniqueResult();
		return pro_id;
	}

	public String signup(SignupRequest request, String userToken)
			throws Exception {
		logger.debug("start signup for LoginDao Implementation .");
		UserDetails userDetail = new UserDetails();
		userDetail.setFname(request.getFname());
		userDetail.setLname(request.getLname());
		userDetail.setEmailId(request.getEmail());
		userDetail.setIs_expired(false);
		userDetail.setExpiredDate("");
		userDetail.setToken(userToken);
		//userDetail.setUserid(request.getEmail());
		userDetail.setEmailVerified(false);
		userDetail.setLoginTokenCreatedDate("");
		userDetail.setCreatedDate(InceptUtils.getDateInString(
				DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
		userDetail.setModifiedDate(InceptUtils.getDateInString(
				DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
		userDetail.setPassword(request.getPassword());
		userDetail.setStatus(AccountStatus.ACTIVE);

		Session session = sessionFactory.getCurrentSession();
		session.persist(userDetail);

		logger.debug("end signup for LoginDao Implementation .");
		return "";
	}
}
