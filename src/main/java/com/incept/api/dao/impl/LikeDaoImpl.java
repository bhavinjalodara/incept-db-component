package com.incept.api.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.dao.LikeDao;
import com.incept.api.model.Like;

@Repository
public class LikeDaoImpl implements LikeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	Logger logger = LoggerFactory.getLogger(LikeDaoImpl.class);

	@Override
	public void createLike(Like like) {
		logger.debug(" Start Like Dao Implementation for create like. ");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(like);
		} catch (Exception exception) {
			logger.error("Error while saving ", exception);
		}
		logger.debug(" End Like Dao Implementation for create like.");
	}

	@Override
	public List<Like> getLike(String postId, String profileId) {
		logger.debug(" Start Dao layer for getting Like .");
		Session session = sessionFactory.getCurrentSession();
		List<Like> likelist = Collections.emptyList();
		try {
			
			Criteria criteria = session.createCriteria(Like.class).add(Restrictions.eq("postId", postId))
					.add(Restrictions.eq("profileId", profileId));
			likelist = criteria.list();
			logger.debug(" End Dao layer for getting Like .");
		} catch (Exception exception) {
			logger.error(" exception while get like from DB . ", exception);
		}
		return likelist;
	}

	@Override
	public Boolean updateLike(Like like) {
		Session session = sessionFactory.getCurrentSession();
		boolean isupdate = false;
		try {
			String sql = "update Like set type=:" + " where  postId=:" + like.getPostId() + " profileId=:" + like.getProfileId();
			int updatedEntities = session.createQuery(sql).setString("postId", like.getPostId())
					.setString("profileId", like.getProfileId()).executeUpdate();
			if (updatedEntities != 1) {
				isupdate = true;
			}
		} catch (Exception e) {
			logger.error(" Exception while fetching the data . ", e);
		}
		return isupdate;
	}

}
