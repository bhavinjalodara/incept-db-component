package com.incept.api.dao.impl;

import java.util.Date;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.dao.LoginDetailsDao;
import com.incept.api.model.LoginDetails;
import com.incept.api.services.enums.AccountStatus;
import com.incept.api.services.enums.DateFormatter;
import com.incept.api.utils.InceptUtils;
import com.incept.api.vo.LoginVO;

@Repository
public class LoginDetailsDaoImpl implements LoginDetailsDao {

	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = LoggerFactory.getLogger(LoginDetailsDaoImpl.class);

	@Override
	public String saveLoginDetail(LoginVO login) {
		logger.debug(" save Login_details for user Id " + login.getUserId());
		LoginDetails loginDetails = new LoginDetails();

		loginDetails.setProfileId(login.getProfileId());
		String loginKey = UUID.randomUUID().toString();
		loginDetails.setLoginKey(loginKey);
		loginDetails.setCreatedDate(InceptUtils.getDateInString(DateFormatter.DATE_IN_SECOND.getFormat(), new Date()));
		loginDetails.setStatus(AccountStatus.ACTIVE.getStatus());
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(loginDetails);
		} catch (Exception e) {
			logger.error("An error occurred while saving login details", e);
			loginKey = null;
		}
		return loginKey;
	}
}
