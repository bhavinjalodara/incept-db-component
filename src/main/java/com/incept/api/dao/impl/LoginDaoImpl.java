package com.incept.api.dao.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incept.api.common.Response;
import com.incept.api.dao.LoginDao;
import com.incept.api.model.UserDetails;
import com.incept.api.services.enums.AccountStatus;
import com.incept.api.vo.LoginVO;

@Repository
public class LoginDaoImpl implements LoginDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = LoggerFactory.getLogger(LoginDaoImpl.class);

	public Response login(String userName, String password) {

		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UserDetails.class);
		// query for database
		Object object = criteria.list();
		return new Response();
	}

	public Object signUp() {
		UserDetails signup = new UserDetails();
		signup.setEmailId("");
		signup.setPassword("");
		Session session = sessionFactory.getCurrentSession();;
		Criteria criteria = session.createCriteria(UserDetails.class);
		Object object = criteria.list();
		return object;
	}

	

	@Override
	public long getProfileId(String userId) throws Exception {
		long profileId = 0L;
		Session session = sessionFactory.getCurrentSession();;
		Criteria creteria = session.createCriteria(UserDetails.class).add(Restrictions.eq("emailId", userId))
				.add(Restrictions.eq("status",AccountStatus.ACTIVE.getStatus())).setProjection(
				Projections.projectionList().add(Projections.property("profile_id"), "profile_id"));
		List<Long> userDetails = creteria.list();
		for (Long userDetail : userDetails) {
			profileId = userDetail;
		}
		return profileId;
	}

	@Override
	public String setkeyData(LoginVO login) {
		// get encrypted key data from the data
		long startTime = System.currentTimeMillis();
		logger.info("start get Key data from DB");
		Session session = sessionFactory.getCurrentSession();;
		String password = null;
		Criteria criteria = session.createCriteria(UserDetails.class).add(Restrictions.eq("profile_id", login.getProfileId()));
		List<UserDetails> listUserDetails = criteria.list();
		for (UserDetails detail : listUserDetails) {
			password = detail.getPassword();
		}
		long endTime = System.currentTimeMillis();
		logger.trace("Time taken for key by DB "
				+ TimeUnit.SECONDS.toSeconds(endTime - startTime));
		logger.info(" End get Key data from DB");
		return password;
	}

	@Override
	public void updateLoginDetail(long profileId, String loginKey) {
		logger.info(" Start update user details for current or last login key history !");
		Session session = sessionFactory.getCurrentSession();;
		String sql = "update UserDetails set lastLoginKey=:loginkey where profile_id=:profileId";

		try {
			session.createQuery(sql).setString("loginkey", loginKey).setLong("profileId", profileId).executeUpdate();
		} catch (Exception e) {
			logger.error("An error occrred while updating login details", e);
		}
		logger.info(" End Update user details for Current or last login key history !");
	}
}
